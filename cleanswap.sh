#!/bin/bash
# Author: Ander
# Usage ./cleanswap
# Clean swap if it's possible
######################

[[ $EUID -eq 0 ]] && echo Cleaning swap... && swapoff -a && \
swapon -a && echo Done || { echo Please, run as root ;	exit \
1 ; }
