# Swap cleaner

Simple script made for cleaning swap partition and speed up your system. You can install it copying the file into `/usr/local/bin` directory in order to allow you use that with just a command after `reset` your terminal. In most GNU/Linux distributions, the given path is included in the variable `$PATH`. If you don't sure, do `echo $PATH | grep "/usr/local/bin"`.
