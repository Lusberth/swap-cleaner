#!/bin/bash

[[ $EUID -ne 0 ]] && echo "Please, run as root" && exit 1

echo "Installing cleanswap..."
cp cleanswap.sh /usr/local/bin/cleanswap
chmod 755 /usr/local/bin/cleanswap
echo "Done"
